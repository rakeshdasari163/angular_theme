import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatMenuModule, MatToolbarModule } from '@angular/material';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports:[
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule
  ]
})
export class MatModule { }
